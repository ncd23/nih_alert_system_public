# NIH_Alert_System_Public

**Overview**
This project includes two Python files that work together to retrieve a list of non-compliant publications from the Public Access Compliance Monitor (PACM) from NIH. The first file, Create_NIH_Database_Table.py, creates a table in an access database that is going to store all of the data retrieved from PACM. The second file, Insert_PACM_Data.py, handles the data pull by calling on the PACM API. It processes  the data using Pandas data frames and inserts the data into the Access database.

**Requirements**
•	Python 3.x
•	pyodbc library
•	pandas library
•	Access database

**How to Use**
1.	Clone or download the project files from GitLab to your local machine.
2.	Make sure that the pyodbc and pandas libraries are installed on your machine. If not, install them using the following commands in your terminal:
pip install pyodbc
pip install pandas
3.	Open Create_NIH_Database_Table.py in a Python IDE, I use PyCharm.
4.	Edit the database connection string with the path to your Access Database. The connection string is located at the top of the script:
conn_string = r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=C:\YOUR_FOLDERS_HERE;’

5.	Run Create_NIH_Database_Table.py to create a table in the Access Database you selected to store the non-compliant publications
6.	Open Insert_PACM_Data.py in your IDE
7.	Once again, edit the database string for your file path.
conn_string = r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=C:\YOUR_FOLDERS_HERE;’

8.	Update the dates in the script to match the desired time period for retrieving non-compliant records.

9.	Update the API URL with your institutions Information as well as your own API key from PACM. You can find your token here: 
https://www.ncbi.nlm.nih.gov/pmc/utils/pacm/token
10.	Lastly run Insert_PACM_Data.py to retrieve the data from PACM and insert them into the database.

**Notes**
•	This project was developed using Python 3.x and may not be compatible with earlier versions of Python.
•	The Access Database must have the table with the same name and columns as specified in the SQL statement in Create_NIH_Database_Table.py. If the table already exists, the script will fail to create it. You should only ever have to run this program once.
•	The Access Database must have the same file path and name as specified in the connection string.   If the file path or name is incorrect then you will not be able to connect to the database so double check for typos.
•	If you have any questions about how to use this feel free to email me at Nathan.dunn@duke.edu
